# Change Log

All notable changes to this project will be documented in this file.

## [0.0.3] - 2021-02-05

### Added

### Changed

- GitLab support

### Fixed

## [0.0.2] - 2021-02-01

### Added

### Changed

- Initializing the project

### Fixed
