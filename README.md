# @ridesz/usual-dependency-graph-checker

The typical dependency graph checking config I use for my TypeScript projects.

## How to use

### `package.json` setup

Just please add this `scripts` tag part to your `package.json` file too:

```json
{
    "test:dependency": "npx @ridesz/usual-dependency-graph-checker",
}
```

### GitLab CI config

Please add the necessary parts to your `.gitlab-ci.yml` file:

```yml
image: node:latest

stages:
    - test

dependecy:
    stage: test
    artifacts:
        paths:
            - build/
        expire_in: 1 week
        when: always
    script:
        - npm install
        - npm rebuild
        - npm run test:dependency
```

This would use the latest node image and do the dependency graph check as part of the `test` stage.

### Running manually


If you would like to run the documentation generation manually then you can use the following command:

```bash
npm run test:dependency
```

## Some implementation details

The result of the check (the graph in SVG and PNG formats) is generated into the `reports/dependency` folder.

The checker only checks the sources from the `src` folder and skips the `*.spec.ts` and `*.spec.tsx` files.
