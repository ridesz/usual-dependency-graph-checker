#!/usr/bin/env node

import shellJs from "shelljs";
import fs from "fs-extra";
import sharp from "sharp";
import { graphviz } from "@hpcc-js/wasm";
import { dependencyCruiserConfig } from "./dependencyCruiserConfig.js";
import { cruise, OutputType, IReporterOutput } from "dependency-cruiser";

const outputFolder = "reports/dependency";

function getProjectFolder(): string {
    const result = shellJs.pwd();
    if (result.code !== 0) {
        throw new Error("Can't get the project folder path");
    }
    return result.stdout;
}

const projectFolder = getProjectFolder();

async function removingOldFolder(): Promise<void> {
    const result = shellJs.exec(`npx rimraf ${projectFolder}/${outputFolder}`);
    if (result.code !== 0) {
        throw new Error("Can't remove the old dependency folder.");
    }
    fs.ensureDirSync(`${projectFolder}/${outputFolder}`);
}

function fixingSvgHrefs(svg: string): string {
    const projectUrl = process.env.CI_PROJECT_URL;
    if (projectUrl !== undefined) {
        // GitLab CI
        return svg.replace(new RegExp('href="src', "g"), 'href="' + projectUrl + "/tree/master/src");
    } else {
        // Not GitLab CI
        return svg.replace(new RegExp('href="src', "g"), 'href="../../src');
    }
}

function runDependencyCruiser(outputType: OutputType): IReporterOutput {
    const sourceFolder = shellJs.pwd() + "/src";
    return cruise([sourceFolder], { ...dependencyCruiserConfig, outputType });
}

async function generateSvgFromDot(dot: string): Promise<string> {
    const svg = await graphviz.layout(dot, "svg", "dot");
    return fixingSvgHrefs(svg);
}

async function renderImages(): Promise<void> {
    const result = runDependencyCruiser("dot");

    if (result.exitCode !== 0) {
        // Error, we don't finish the image creation
        return;
    }

    if (typeof result.output !== "string") {
        // This should never happen.
        throw new Error("Wrong dependency cruiser output type");
    }

    const svg = await generateSvgFromDot(result.output);

    // Save SVG
    fs.writeFileSync(`${projectFolder}/${outputFolder}/dependency.svg`, svg);
    fs.writeFileSync(
        `${projectFolder}/${outputFolder}/dependency.html`,
        "<html><head><title>Dependency graph</title></head><body>" + svg + "</body></html>",
    );

    // Save PNG
    await sharp(Buffer.from(svg)).png().toFile(`${projectFolder}/${outputFolder}/dependency.png`);
}

function checkDependencies(): void {
    const result = runDependencyCruiser("err");

    console.log(result.output);

    if (result.exitCode !== 0) {
        throw new Error("Dependency cruiser had some error!");
    }

    if (String(result.output).includes("✖")) {
        throw new Error("Error cross detected! (Dependency cruiser error.)");
    }
}

async function main(): Promise<void> {
    try {
        console.log("Generating dependency graphs");

        await removingOldFolder();

        await renderImages();

        checkDependencies();

        console.log();
        console.log("Dependency graph generation finished.");
        console.log();
        shellJs.exit(0);
    } catch (error) {
        console.log();
        console.log(`ERROR WITH DEPENDENCY GRAPH GENERATION: ${(error as Error).message}`);
        console.log();
        console.log((error as Error).stack);
        console.log();
        shellJs.exit(1);
    }
}

main();
